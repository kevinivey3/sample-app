class StaticPagesController < ApplicationController
  def home
  end

  def help
  end
  
  def about
  end
  
  def contact
  end
  
  def comingSoon
  end
  
  def leaderboard
  end
end
