json.array!(@factions) do |faction|
  json.extract! faction, :id, :name, :numMembers, :currentGame, :admin
  json.url faction_url(faction, format: :json)
end
