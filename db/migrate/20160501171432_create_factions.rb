class CreateFactions < ActiveRecord::Migration
  def change
    create_table :factions do |t|
      t.string :name
      t.integer :numMembers
      t.string :currentGame
      t.string :admin

      t.timestamps null: false
    end
  end
end
